package es.meriland.cuentaAtras;

import net.md_5.bungee.api.plugin.Plugin;
import java.util.logging.Level;
import net.md_5.bungee.api.plugin.Listener;


public class CuentaAtras extends Plugin implements Listener {
    
    
    @Override
    public void onEnable() {
        this.getProxy().getPluginManager().registerListener(this, new ListenerC(this));
        this.getProxy().getLogger().log(Level.INFO, "Plugin de Cuenta Atrás iniciado");
        
    }

    
}
