package es.meriland.cuentaAtras;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;


public class ListenerC implements Listener {
    
    private final CuentaAtras plugin;
    
    public ListenerC(CuentaAtras plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler
    public void onProxyPing(ProxyPingEvent event) {
        ServerPing sp = event.getResponse();
        DateFormat formatter = SimpleDateFormat.getTimeInstance();
        formatter.setTimeZone(TimeZone.getTimeZone("Europe/Copenhagen"));
        
        try {
            Date dia = toFecha(getFechaApertura());
            Date fecha = sumarHoras(dia, 17);
            sp.setDescription("§6Abrimos de nuevo en §a"+getTiempoRestante(fecha, 4)+"§3 días, §a"+getTiempoRestante(fecha,3)+"§3 horas y §a"+getTiempoRestante(fecha,2)+ "§3 minutos §5:D");
        } catch (ParseException e) {
            sp.setDescription("Estamos en mantenimiento!");
        }
    }
    public String getFechaApertura(){
        return "19/12/14";   
    }
    public Date toFecha(String s) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy"); //Formato de la fecha
        Date date = sdf.parse(s);
        return date; //Devolvemos la fecha
    }
    public String fechaToString(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy"); //Formato de la fecha
	String date = sdf.format(fecha); //Formateamos a String la decha
	return date;
    }
    public Date sumarHoras(Date fecha, int horas){
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(fecha); // Configuramos la fecha que se recibe	
      calendar.add(Calendar.HOUR, horas);  
      return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos    
    }
    public Date sumarMin(Date fecha, int min) {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(fecha); // Configuramos la fecha que se recibe	
      calendar.add(Calendar.MINUTE, min);  
      return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos     
    }
    public long getTiempoRestante(Date fechafin, int i){
        Date hoy;
        Date fin;
        try {
            hoy = new Date();
            fin = fechafin;
            //Codigo de 'mkyong'
            long diff = fin.getTime()- hoy.getTime();
 
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            
            switch(i){
                case 1: return diffSeconds;
                case 2: return diffMinutes;
                case 3: return diffHours;
                case 4: return diffDays;    
            }
        } catch (Exception e){ }
        return -1;
    }

    
}
